## To be updated


---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


---

## Quick Steps

1. After cloning the repository

```
$ cd exp_loc_map
$ git submodule update --init --recursive
$ cd localization/
$ catkin_make
$ source devel/setup.bash
```

2. To run the fast_lio, in the commandline enter the following command. 

```
$ roslaunch fast_lio_launcher fast_lio.launch lidar_pos_arg:="f" rviz:=true
```
This will run the node for the front lidar.

3. "lidar_pos_arg" are f, fl, fr, rl and rr for front, front_left, front_right, rear_left and rear_right respectively.
4.  To run the node for other lidars, eg. for the front_left_lidar, in the terminal enter,

```
$ roslaunch fast_lio_launcher fast_lio.launch lidar_pos_arg:="fl" rviz:=false

```
    
